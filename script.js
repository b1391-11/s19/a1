function getCube(num){
    let cube = num ** 3;

    console.log(`The cube of ${num} is ${cube}`);
}

getCube(2);

function printAddress(address){
    let [addressLine1, city, state, zip] = address; 
    console.log(`I live at ${addressLine1}, ${city}, ${state}, ${zip}.`);

}

address = ["1234 Canon Road", "Los Angeles", "California", "90001"];

printAddress(address);

function printAnimal(animal){

    const {name, description, weight, feet, inches} = animal;

    console.log(`${name} was a ${description}. He weighed ${weight} kgs with a measurement of ${feet} ft ${inches} in.`);
}

let animal = {
    name: "Lolong",
    description: "Saltwater Crocodile",
    weight: 1075,
    feet: 20,
    inches: 3
};

printAnimal(animal);

let number = [1, 2, 3, 4, 5];

number.forEach((num) => {
    console.log(num);
});

let sum = number.reduce((total, num) => {
    return total + num;
});

console.log(sum);

function Dog(name, age, breed){
    this.name = name;
    this.age = age;
    this.breed = breed;
}

let dog1 = new Dog("Pakun", 1, "French Bulldog");

console.log(dog1);
